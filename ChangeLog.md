# Change Log

[TOC]

# Release 1.2 - 2024-05-27

## Features

- Display drawing in svg format in jupyter
- Handle file input with FileDraw ([Issue #1](https://github.com/anthonyhtyao/varnaapi/issues/1))
- Logging system and move cmd printing to Debug ([Issue #2](https://github.com/anthonyhtyao/varnaapi/issues/2))
